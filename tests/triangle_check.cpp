#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../source/Triangle.h"


class TriangleFixture : public ::testing::Test {
protected:
    virtual void TearDown() {
      //  delete triangle;
    }

    virtual void SetUp() {
        //triangle = new Triangle(0.0, 0.0, 0.0, 0.0, 0.0,0.0);
    }

    //Triangle* triangle;
};


TEST_F(TriangleFixture, First) {

    Point A {4.0, 5.0}, B{-2.0, 3.0}, C{2.0, 1.0};
    Triangle triangle(A,B,C);

    //EXPECT_EQ(triangle.getArea(), 10.0);
    //EXPECT_EQ(A.getX(), 4.0);
    EXPECT_EQ((int)triangle.getArea(), 10);
}