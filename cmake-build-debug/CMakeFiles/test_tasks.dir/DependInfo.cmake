# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/sharkinattor/projects/test_tasks/source/Point.cpp" "/home/sharkinattor/projects/test_tasks/cmake-build-debug/CMakeFiles/test_tasks.dir/source/Point.cpp.o"
  "/home/sharkinattor/projects/test_tasks/source/Triangle.cpp" "/home/sharkinattor/projects/test_tasks/cmake-build-debug/CMakeFiles/test_tasks.dir/source/Triangle.cpp.o"
  "/home/sharkinattor/projects/test_tasks/tests/triangle_check.cpp" "/home/sharkinattor/projects/test_tasks/cmake-build-debug/CMakeFiles/test_tasks.dir/tests/triangle_check.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/googletest/googlemock/include"
  "../lib/googletest/googletest/include"
  "../lib/googletest/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/sharkinattor/projects/test_tasks/cmake-build-debug/lib/googletest/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/sharkinattor/projects/test_tasks/cmake-build-debug/lib/googletest/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
