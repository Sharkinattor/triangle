#include <iostream>
#include "Point.h"
#include "Triangle.h"

void displayTriangleData(const Triangle& triangle);
void printTriangleType(const Triangle& triangle);

int main()
{
    {
        Point A(4.0, 5.0);
        Point B(-2.0, 3.0);
        Point C(2.0, 1.0);

        Triangle triangle(A,B,C);

        displayTriangleData(triangle);

        printTriangleType(triangle);

    }

    std::cout << "\n" << "/****************************************************************/" << std::endl;

    {
        Point A(0.0, 0.0);
        Point B(4.0, 0.0);
        Point C(0.0, 4.0);

        Triangle triangle(A,B,C);

        displayTriangleData(triangle);

        printTriangleType(triangle);

    }

    std::cout << "\n" << "/****************************************************************/" << std::endl;

    {
        Point A(8.0, -1.0);
        Point B(5.0, -4.0);
        Point C(2.0, 3.0);

        Triangle triangle(A,B,C);

        displayTriangleData(triangle);

        printTriangleType(triangle);

    }

    std::cout << "\n" << "/****************************************************************/" << std::endl;

    {
        Point A(-4.0, -1.0);
        Point B(0.0, 4.0);
        Point C(5.0, 7.0);

        Triangle triangle(A,B,C);

        displayTriangleData(triangle);

        printTriangleType(triangle);

    }

    std::cout << "\n" << "/****************************************************************/" << std::endl;

    {
        Triangle triangle(0.0, 4.0, -1.0, 5.0, 3.0, 2.0);

        displayTriangleData(triangle);

        printTriangleType(triangle);

    }

    std::cout << "\n" << "/****************************************************************/" << std::endl;

    {
        Triangle triangle(7.0, 5.0, 1.0, 2.0, -4.0, -5.0);

        displayTriangleData(triangle);

        printTriangleType(triangle);
    }

    return 0;
}


void displayTriangleData(const Triangle& triangle)
{
    std::cout << "Perimeter: " << triangle.getPerimeter() << std::endl;
    std::cout << "Area: " << triangle.getArea() << std::endl;

    std::cout << "A: " << triangle.getSide12Length() << std::endl;
    std::cout << "B: " << triangle.getSide23Length() << std::endl;
    std::cout << "C: " << triangle.getSide13Length() << std::endl;

    std::cout << "Degrees in radians:" << std::endl;
    std::cout << "Alpha: " << triangle.getAngle1() << std::endl;
    std::cout << "Beta: " << triangle.getAngle2() << std::endl;
    std::cout << "Gamma: " << triangle.getAngle3() << std::endl;

}

void printTriangleType(const Triangle& triangle)
{
    if (triangle.isEquilateral())
        std::cout << "Equilateral";
    else if (triangle.isIsosceles())
        std::cout << "Isosceles";
    else if (triangle.isRectangular())
        std::cout << "Rectangular";
    else
        std::cout << "Versatile" << std::endl;
}