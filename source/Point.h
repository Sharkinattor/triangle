#ifndef TEST_TASKS_POINT_H
#define TEST_TASKS_POINT_H

#include <iostream>

class Point {
public:

    Point();
    Point(double x, double y);

    double getX() const { return x_; };
    double getY() const { return y_; };


private:
    double x_;
    double y_;
};

bool operator== (const Point& lhs, const Point& rhs);
bool operator!= (const Point& lhs, const Point& rhs);
Point operator+ (const Point& lhs, const Point& rhs);
Point operator- (const Point& lhs, const Point& rhs);
std::ostream& operator<<(std::ostream& os, const Point& point);


#endif //TEST_TASKS_POINT_H
