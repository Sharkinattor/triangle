#ifndef TEST_TASKS_TRIANGLE_H
#define TEST_TASKS_TRIANGLE_H

#include <iostream>
#include "Point.h"

class Triangle {

public:
    Triangle(double x1, double y1, double x2, double y2, double x3, double y3);

    Triangle(Point point1, Point point2, Point point3);

    Point getPoint1() const;
    Point getPoint2() const;
    Point getPoint3() const;

    double getSide12Length() const;
    double getSide23Length() const;
    double getSide13Length() const;

    double getPerimeter() const;

    double getArea() const;

    double getAngle1() const;
    double getAngle2() const; //RADIANS
    double getAngle3() const;

    bool isRectangular() const;
    bool isIsosceles() const;
    bool isEquilateral() const;


protected:
    void checkSides() const;
private:
    Point point1, point2, point3;
};

bool operator== (const Triangle& lhs, const Triangle& rhs);
bool operator!= (const Triangle& lhs, const Triangle& rhs);


#endif //TEST_TASKS_TRIANGLE_H
