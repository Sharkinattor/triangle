#include "Point.h"

Point::Point()
    :x_(0.0), y_(0.0)
{

}

Point::Point(double x, double y)
    : x_(x), y_(y)
{

}

bool operator== (const Point& lhs, const Point& rhs)
{
    return (lhs.getX() == rhs.getX()) && (lhs.getY() == rhs.getY());
}

bool operator!= (const Point& lhs, const Point& rhs)
{
    return !(lhs == rhs);
}

Point operator+ (const Point& lhs, const Point& rhs)
{
    return Point(lhs.getX() + rhs.getX(), lhs.getY() + rhs.getY());
}

Point operator- (const Point& lhs, const Point& rhs)
{
    return Point(lhs.getX() - rhs.getX(), lhs.getY() - rhs.getY());
}

std::ostream& operator<<(std::ostream& os, const Point& point)
{
    os << '(' << point.getX() << ',' << point.getY() << ')';
    return os;
}
