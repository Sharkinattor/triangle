#include <cmath>
#include "Triangle.h"

const double Degree_90 = 1.57;

Triangle::Triangle(double x1, double y1, double x2, double y2, double x3, double y3)
    : point1(x1, y1), point2(x2, y2), point3(x3, y3)
{
    checkSides();
}

Triangle::Triangle(Point p1, Point p2, Point p3)
    :point1(p1), point2(p2), point3(p3)
{
    checkSides();
}

Point Triangle::getPoint1() const { return point1; }

Point Triangle::getPoint2() const { return point2; }

Point Triangle::getPoint3() const { return point3; }

double Triangle::getSide12Length() const
{
    return sqrt(pow(getPoint2().getX() - getPoint1().getX() ,2) + pow(getPoint2().getY() - getPoint1().getY() ,2));
}

double Triangle::getSide23Length() const
{
    return sqrt(pow(getPoint3().getX() - getPoint2().getX(), 2) + pow(getPoint3().getY() - getPoint2().getY(), 2));
}

double Triangle::getSide13Length() const
{
    return sqrt(pow(getPoint3().getX() - getPoint1().getX() ,2) + pow(getPoint3().getY() - getPoint1().getY() ,2));
}

double Triangle::getPerimeter() const
{
    return (std::abs(getSide12Length()) + std::abs(getSide13Length()) + std::abs(getSide23Length()));
}

double Triangle::getArea() const {
    double p = getPerimeter() / 2;
    return sqrt(p * ((p - getSide12Length()) * (p - getSide23Length())) * (p - getSide13Length()));
}

double Triangle::getAngle1() const
{
    double cos_aplha;
    cos_aplha = (std::abs(pow(getSide12Length(), 2)) + std::abs(pow(getSide13Length(), 2)) - std::abs(pow(getSide23Length(), 2)))
            / (2 * std::abs(getSide12Length()) * std::abs(getSide13Length()));
    //return (acos(cos_aplha) * 180 / 3.14);
    return acos(cos_aplha);
}

double Triangle::getAngle2() const
{
    double cos_beta;
    cos_beta = (std::abs(pow(getSide12Length(), 2)) + std::abs(pow(getSide23Length(), 2)) - std::abs(pow(getSide13Length(), 2)))
            / (2 * std::abs(getSide12Length()) * std::abs(getSide23Length()));
    return acos(cos_beta);
//    return (acos(cos_beta) * 180 / 3.14);
}

double Triangle::getAngle3() const
{
    double cos_gamma;
    cos_gamma = (std::abs(pow(getSide13Length(), 2)) + std::abs(pow(getSide23Length(), 2)) - std::abs(pow(getSide12Length(), 2)))
            / (2 * std::abs(getSide13Length()) * std::abs(getSide23Length()));
    return acos(cos_gamma);
//    return (acos(cos_gamma) * 180 / 3.14);
}

bool Triangle::isRectangular() const {
    return (getAngle1() == Degree_90) || (getAngle2() == Degree_90) || (getAngle3() == Degree_90);
}

bool Triangle::isIsosceles() const {
    if(getSide12Length() == getSide23Length())
        return true;
    if(getSide23Length() == getSide13Length())
        return true;
    if(getSide13Length() == getSide12Length())
        return true;

    return false;
}

bool Triangle::isEquilateral() const {
    return (getSide13Length() == getSide12Length() == getSide23Length());
}

void Triangle::checkSides() const {
    if(!(getSide12Length() + getSide23Length() > getSide13Length()) &&
       !(getSide12Length() + getSide13Length() > getSide23Length()) &&
       !(getSide23Length() + getSide13Length() > getSide12Length()))
    {
        std::cout << " *** You can't create a triangle with the given coordinates! ***" << std::endl;
        //exit(EXIT_FAILURE);
    }

    if(getPoint1() == getPoint2() || getPoint1() == getPoint3() || getPoint2() == getPoint3())
    {
        std::cout << "*** Some sides are the same! ***" << std::endl;
        //exit(EXIT_FAILURE);
    }

    if(getPoint1().getY() == getPoint2().getY() == getPoint3().getY())
    {
        std::cout << "*** Same top! ***" << std::endl;
        //exit(EXIT_FAILURE);
    }

    if(getPoint1().getX() == getPoint2().getX() == getPoint3().getX()) {
        std::cout << "*** Point are on the same axis! ***" << std::endl;
        //exit(EXIT_FAILURE);
    }
}

bool operator== (const Triangle& lhs, const Triangle& rhs)
{
    return (lhs.getPoint1() == rhs.getPoint1()) && (lhs.getPoint2() == rhs.getPoint2()) && (lhs.getPoint3() == rhs.getPoint3());
}

bool operator!= (const Triangle& lhs, const Triangle& rhs)
{
    return !(lhs == rhs);
}